<h1 align="center">Electronic Portfolio</h1>

# Обо мне:

Всем привет! В данный момент вы находитесь на электронной версии моего драгоценного портфолио. Меня зовут Даша и ниже вы можете увидеть парочку интересных фактов обо мне…

Личные качества:
- Стремление к профессиональному совершенствованию и постоянное обучение.
- Творческий и аналитический подход к решению задач.
- Ответственность и аккуратность в выполнении поставленных задач.
- Высокая степень самоорганизации и эффективное управление временем.

Знание языков:
- Английский: продвинутый уровень, свободное владение технической лексикой.

Образование:
- студентка ИГУ ФБКИ, направление  - Прикладная Информатика в дизайне,
- Детская Художественная Школа №3 г. Иркутск.

Профессиональные Навыки:
- Продвинутые знания Python.
- Уверенное владение Php и Kotlin.
- Знание современных методологий разработки, включая Agile и Scrum.
- Командный игрок с высоким уровнем коммуникативных навыков.

Знание программ:
- Adobe Photoshop,
- Figma,
- CoreI Draw,
- Animate,
- DaVinci Resolve,
- CapCut,
- TouchDesigner,
- Tiled.

Дополнительные навыки:
- Опыт работы с базой данных MySQL.
- Знание фреймворка для веб-разработки, такого как Flask.

# Перейдя по данным ссылкам, вы можете ознакомиться с моими работами:

<h3 align="center"><a href="https://gitlab.com/darya3141590/electronic-portofolio/-/blob/main/4th_semester/web_application_design/final_project.pdf">Reserve Website Design</a></h3>

<h3 align="center"><a href="https://gitlab.com/darya3141590/electronic-portofolio/-/blob/main/2nd_semester/summer_practice/summer_practice.pdf">Summer practice 1st year</a></h3>

<h3 align="center"><a href="https://gitlab.com/darya3141590/electronic-portofolio/-/blob/main/4th_semester/summer_practice/practice.pdf">Summer practice 2nd year</a></h3>

<h3 align="center"><a href="https://gitlab.com/darya3141590/electronic-portofolio/-/tree/main/5th_semester/design_of_gui">Design of GUI</a></h3>

<h3 align="center"><a href="https://gitlab.com/darya3141590/electronic-portofolio/-/blob/main/5th_semester/publishing_and_design/publishing_and_design.pdf">Publishing and Design</a></h3>

<h3 align="center"><a href="https://gitlab.com/darya3141590/electronic-portofolio/-/tree/main/4th_semester/visual_identity">Visual Identity</a></h3>

<h3 align="center"><a href="https://gitlab.com/darya3141590/electronic-portofolio/-/blob/main/2nd_semester/graphics/tasks.pdf">Computer Graphic</a></h3>

<h3 align="center"><a href="https://gitlab.com/darya3141590/electronic-portofolio/-/blob/main/3rd_semester/multimedia_technology_and_animation/tasks.pdf">Multimedia Technology and Animation</a></h3>

<h3 align="center"><a href="https://gitlab.com/darya3141590/electronic-portofolio/-/tree/main/3rd_semester/computer_math">Computer Mathematics</a></h3>
